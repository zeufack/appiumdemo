package com.appiumdemo.steps;

import com.appiumdemo.pagesObject.Home;

import io.cucumber.java8.En;

public class FillHomeFormStep implements En {
    public FillHomeFormStep(Home home) {
        Given("I am on home page", () -> {
            home.clickOnToggleButton();
            home.clickOnBackButton();
        });
        When("Click on proceed Button", () -> {
            home.fillServerUrl("https://api.dev.bazzptt.com");
            home.fillSim("334454565");
            home.clickOnProcedeButton();
        });
        Then("I get bat format", () -> {
            // Write code here that turns the phrase above into concrete actions
        });
    }
}
