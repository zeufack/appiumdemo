package com.appiumdemo.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;

import java.net.MalformedURLException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.appiumdemo.client.ServerManager;

// Base Runner class is use to connect cucumber an testng.
public class BaseRunner extends AbstractTestNGCucumberTests {

    /**
     * Setup step goes here.
     * 
     * @throws MalformedURLException
     */
    @BeforeSuite
    public void beforSuite() throws MalformedURLException {
        System.out.println("befor suite");
        ServerManager.INSTATCE.setDriver("emulator-5554",
                "C:/Users/PZeufack/Documents/programming/appiumdemo/src/test/resources/apk/app-debug.apk");
    }

    /**
     * Teardown step goes here.
     */
    @AfterSuite
    public void afterSuite() {
        System.out.println("after suit code");
    }

}
