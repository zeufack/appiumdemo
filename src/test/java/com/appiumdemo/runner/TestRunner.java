package com.appiumdemo.runner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {
        "pretty",
        "html:target/reports/html/htmlreport",
        "json:target/reports/jsonreports/index.json",
        "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"
}, features = { "src/test/resources/features" }, glue = { "com.appiumdemo.steps" })
// Tell Cucumber where to find features and steps.
public class TestRunner extends BaseRunner {

}
