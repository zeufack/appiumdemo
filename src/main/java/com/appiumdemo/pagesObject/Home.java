package com.appiumdemo.pagesObject;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class Home extends View {

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.Switch")
    private MobileElement toggleSettingButton;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Back\"]")
    private MobileElement backButton;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText[1]")
    private MobileElement systemUrl;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText[2]")
    private MobileElement simProcede;

    @AndroidFindBy(xpath = "//android.widget.Button[@content-desc=\"PROCEED\"]")
    private MobileElement procedeButton;

    /**
     * Cette function a un probleme, si l'application a déjà ouvert une fois,
     * l'élément ne s'affiche pas.
     */
    public void clickOnToggleButton() {
        this.loadingWait.until(elementToBeClickable(this.toggleSettingButton)).click();
    }

    public void clickOnBackButton() {
        this.loadingWait.until(elementToBeClickable(this.backButton)).click();
    }

    public void clickOnProcedeButton() {
        this.loadingWait.until(elementToBeClickable(this.procedeButton)).click();
    }

    public void fillServerUrl(String url) {
        this.loadingWait.until(elementToBeClickable(this.systemUrl)).clear();
        this.loadingWait.until(elementToBeClickable(this.systemUrl)).sendKeys(url);

    }

    public void fillSim(String simNumber) {
        this.loadingWait.until(elementToBeClickable(this.simProcede)).clear();
        this.loadingWait.until(elementToBeClickable(this.simProcede)).sendKeys(simNumber);

    }

}
